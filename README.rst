Como colocar para rodar
=======================

Este tutorial destina-se a fazer a instalação com o uso do docker e docker-compose, logo você precisará ter estas ferramentas instaladas, segue o site das ferramentas:

* docker
   * Site, http://docker.com
   * Documentação de como instalar em ambientes derivados do debian, https://docs.docker.com/engine/installation/linux/docker-ce/debian/
* docker-compose
   * Overview, https://docs.docker.com/compose/overview/
   * Documentação de como instalar https://docs.docker.com/compose/install/

Tendo as ferramentas instaladas podemos fazer o download do arquivo `tikal-dev-test.tar.gz <https://bitbucket.org/rodrigopmatias/pstt2/downloads/tikal-dev-test.tar.gz>`_ e descompactar em um diretório onde você tenha permissão de escrita.

Com esta descompactação será criando um arquivo docker-compose.yml, verifique a linha 6 deste arquivo, caso esteja usando o usuário root para rodar o sistema, substitua a string "[user id]:[group id] por "0:0" se tiver usando outro usuário rode o comando:

.. code:: sh

  $ echo "$(id -u):$(id -g)"
  1000:1000

Neste caso você irá substituir "[user id]:[group id]" por "1000:1000".

O proximo passo é fazer a construção da image que sera usada pelo docker, para isto rode o comando:

.. code:: sh

  $ docker-compose build app

A descrição desta imagem esta no diretório image/app. Agora você pode rodar o comando:

.. code:: sh

  $ docker-compose up

Caso seja a primeira vez que você esteja rodando o comando acima pode ser que leve algum tempo para instalar as do sistema (django e django-restfraework) além deste pacote será instalado a nossa implementação. No final deste processo será disponibilizado os serviços nos endereços:

* http://localhost:8000/admin/
* http://localhost:8000/api/

Nesta ordem o Admin e API Restful.

Como usar o Sistema
===================

Foram criado dois usuário:

* user1, usuário para fazer o registro das modificações no processo, autorizado a acessar o admin;
* user2, usuário para fazer o cadastro e deleção de processos atraves da API disponibilizada;
* admin, usuário administrado geral do sistema tendo inclusive as funcionalidades:
   * criar empresa
   * criar cadastrar usuários e configurar permissões

Foi produzido um vídeo demostrado o uso do sistema que esta no youtube.
