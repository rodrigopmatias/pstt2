# -*- coding: utf-8 -*-
from threading import current_thread


_current_thread = current_thread()


class CurrentUser(object):

    def __init__(self, get_response):
        self.get_response = get_response

    @classmethod
    def get(klass):
        return getattr(_current_thread, 'current_user', None)

    @classmethod
    def set(klass, user):
        setattr(_current_thread,  'current_user', user)

    def __call__(self, request):
        response = self.get_response(request)
        CurrentUser.set(request.user)
        return response


class __WithCurrentUser:

    def __init__(self, current_user):
        self.user = current_user

    def __enter__(self):
        self.memory = CurrentUser.get()
        CurrentUser.set(self.user)

    def __exit__(self, *args, **kwargs):
        CurrentUser.set(self.memory)


active_user = __WithCurrentUser
