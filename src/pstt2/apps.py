from django.apps import AppConfig

__version__ = '0.0.1'


class DefaultConfig(AppConfig):

    name = 'pstt2'

    verbose_name = 'Acompanhamento de Processos'
