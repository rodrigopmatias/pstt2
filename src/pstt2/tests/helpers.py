# -*- coding: utf-8 -*-
from http.server import BaseHTTPRequestHandler


class MockHttpRequestHandler(BaseHTTPRequestHandler):

    def do_HEAD(self):
        print('DO HEAD')
        self.send_response(200)
        self.end_headers()

    def do_OPTIONS(self):
        print('DO OPTIONS')

    def do_GET(self):
        pass

    def do_POST(self):
        print('do POST')
        self.send_response(200)
        self.end_headers()

    def do_PUT(self):
        pass

    def do_DELETE(self):
        pass
