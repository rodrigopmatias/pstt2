# -*- coding: utf-8 -*-
import multiprocessing as mp

from urllib.request import (Request, urlopen)
from http.server import HTTPServer
from .helpers import MockHttpRequestHandler
from unittest import TestCase
from django.db.utils import IntegrityError
from ..middleware import active_user
from ..models import (
    Company, CustomUser, Process, Notification
)


class Mock:
    pass


mock = Mock()


def setUpModule():
    mock.main_company = Company.objects.create(
        name='Main Company',
        hook='http://localhost:2000/update/',
        authorization='Basic fakeauth'
    )

    mock.common_user = CustomUser.objects.create(
        username='common',
        email='common@user.com',
        first_name='Common',
        last_name='User',
        is_staff=True,
        is_superuser=False
    )

    mock.super_user = CustomUser.objects.create(
        username='super',
        email='super@user.com',
        first_name='Super',
        last_name='User',
        is_staff=True,
        is_superuser=True
    )

    mock.company_user = CustomUser.objects.create(
        username='company',
        email='company@user.com',
        first_name='Company',
        last_name='User',
        is_staff=True,
        is_superuser=True,
        company=mock.main_company
    )


class NotificationTestCase(TestCase):

    def test_minimal_fields(self):
        self.assertTrue(hasattr(Notification, 'process'))
        self.assertTrue(hasattr(Notification, 'hook'))
        self.assertTrue(hasattr(Notification, 'authorization'))
        self.assertTrue(hasattr(Notification, 'at_moment'))
        self.assertTrue(hasattr(Notification, 'status'))


class ProcessTestCase(TestCase):

    def httpd_handle(self):
        httpd = HTTPServer(('127.0.0.1', 2000), MockHttpRequestHandler)
        httpd.serve_forever()

    def setUp(self):
        self.httpd_process = mp.Process(target=self.httpd_handle)
        self.httpd_process.start()

    def tearDown(self):
        self.httpd_process.terminate()

    def test_notify(self):
        req = Request(
            'http://localhost:2000/update/',
            {'a': 1},
            headers={
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        )

        with urlopen(req) as fd:
            print(fd.status)

    def test_detect_is_dirty_with_non_saved(self):
        with active_user(mock.company_user):
            p = Process(
                process_number='002',
                process_data='initial'
            )

            self.assertTrue(p.is_dirty)

    def test_detect_is_dirty_after_restored(self):
        with active_user(mock.company_user):
            p = Process(
                process_number='002',
                process_data='initial'
            )

            p.save()
            older = Process.objects.get(pk=p.pk)
            self.assertFalse(older.is_dirty)

            p.delete()

    def test_detect_is_dirty_after_changed(self):
        with active_user(mock.company_user):
            p = Process(
                process_number='002',
                process_data='initial'
            )

            p.save()
            p.process_data += '\nnew change'
            self.assertTrue(p.is_dirty)

            p.delete()

    def test_detect_is_dirty_after_saved(self):
        with active_user(mock.company_user):
            p = Process(
                process_number='002',
                process_data='initial'
            )

            p.save()
            self.assertFalse(p.is_dirty)

            p.delete()

    def test_create_with_user_without_company(self):
        with active_user(mock.common_user):
            try:
                p = Process.objects.create(
                    process_number='001',
                    process_data='initial'
                )
            except Exception as e:
                self.assertEqual(e.__class__, IntegrityError)
            else:
                self.assertEqual(False, True, 'Deveria ter ocorrido uma exceção')

    def test_create_with_user_with_company(self):
        with active_user(mock.company_user):
            try:
                p = Process.objects.create(
                    process_number='001',
                    process_data='initial'
                )
            except Exception as e:
                self.assertEqual(True, False, e)
            else:
                self.assertNotEqual(p, None)
                p.delete()

    def test_minimal_fields(self):
        self.assertTrue(hasattr(Process, 'process_number'))
        self.assertTrue(hasattr(Process, 'process_data'))
        self.assertTrue(hasattr(Process, 'hash_process_data'))


class CustomUserTestCase(TestCase):

    def test_creation_without_company(self):
        self.assertTrue(CustomUser.objects.filter(company=None).exists())

    def test_minimal_fields(self):
        self.assertTrue(hasattr(mock.common_user, 'username'))
        self.assertTrue(hasattr(mock.common_user, 'email'))
        self.assertTrue(hasattr(mock.common_user, 'first_name'))
        self.assertTrue(hasattr(mock.common_user, 'last_name'))
        self.assertTrue(hasattr(mock.common_user, 'is_superuser'))

    def teste_short_name(self):
        self.assertEqual(mock.common_user.get_short_name(), 'Common')
        self.assertEqual(mock.super_user.get_short_name(), 'Super')
        self.assertEqual(mock.company_user.get_short_name(), 'Company')

    def teste_full_name(self):
        self.assertEqual(mock.common_user.get_full_name(), 'Common User')
        self.assertEqual(mock.super_user.get_full_name(), 'Super User')
        self.assertEqual(mock.company_user.get_full_name(), 'Company User')


class CompanyTestCase(TestCase):

    def test_related_users(self):
        self.assertTrue(mock.main_company.users.exists())
        self.assertEqual(mock.main_company.users.count(), 1)

    def test_minimal_fields(self):
        self.assertTrue(hasattr(mock.main_company, 'name'))
        self.assertTrue(hasattr(mock.main_company, 'hook'))
        self.assertTrue(hasattr(mock.main_company, 'authorization'))
