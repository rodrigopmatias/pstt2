# -*- coding: utf-8 -*-
from unittest import TestCase
from .. import middleware
from .. import models


class CurrentUserTestCase(TestCase):

    @classmethod
    def setUpClass(klass):
        klass.user1 = models.CustomUser.objects.create(
            username='one',
            email='one@user.com',
            first_name='One',
            last_name='User'
        )

    @classmethod
    def tearDownClass(klass):
        klass.user1.delete()

    def test_user_context(self):
        self.assertEqual(middleware.CurrentUser.get(), None)

        with middleware.active_user(self.user1):
            self.assertEqual(middleware.CurrentUser.get(), self.user1)

        self.assertEqual(middleware.CurrentUser.get(), None)

    def test_user_set(self):
        middleware.CurrentUser.set(self.user1)
        self.assertEqual(middleware.CurrentUser.get(), self.user1)
        middleware.CurrentUser.set(None)
        self.assertEqual(middleware.CurrentUser.get(), None)

    def test_not_user_set(self):
        self.assertEqual(middleware.CurrentUser.get(), None)
