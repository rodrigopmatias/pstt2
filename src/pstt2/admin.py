from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .models import Company, CustomUser, Process


@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'email', 'first_name', 'last_name', 'company')}),
        (_('Configurations'), {
            'classes': ('collapse',),
            'fields': ('is_staff', 'is_superuser')
        }),
        (_('Security'), {
            'classes': ('collapse',),
            'fields': ('password',)
        }),
        (_('Permissions'), {
            'classes': ('collapse',),
            'fields': ('groups', 'user_permissions')
        })
    )

    list_display = (
        'username', 'email', 'first_name',  'last_name', 'company'
    )

    list_filter = (
        'is_staff', 'is_superuser', 'company'
    )

    def save_model(self, request, obj, form, change):
        if 'password' in request.POST:
            obj.set_password(request.POST.get('password'))
        super(CustomUserAdmin, self).save_model(request, obj, form, change)


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'hook'
    )


class MyCompaniesListFilter(admin.SimpleListFilter):

    title = _('Company')

    parameter_name = 'my_company'

    def lookups(self, request, model_admin):
        if request.user.is_superuser:
            return [
                (company.pk, str(company))
                for company in Company.objects.filter()
            ]

        return tuple()

    def queryset(self, request, queryset):
        if self.value():
            queryset = queryset.filter(company=self.value())

        return queryset


@admin.register(Process)
class ProcessAdmin(admin.ModelAdmin):
    fields = ('process_number', 'process_data')

    readonly_fields = ('process_number',)

    list_display = (
        'process_number', 'company', 'hash_process_data'
    )

    list_filter = (MyCompaniesListFilter,)

    def get_queryset(self, request):
        qset = super(ProcessAdmin, self).get_queryset(request)

        if not request.user.is_superuser:
            qset = qset.filter(company=request.user.company)

        return qset
