# -*- coding: utf-8 -*-
from rest_framework import serializers, viewsets, mixins
from rest_framework.response import Response
from .models import Process


class ProcessSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Process
        fields = ('pk', 'process_number', 'process_data', 'hash_process_data', 'company_id', 'url')
        readyonly_fields = ('company', 'hash_process_data',)


class ProcessViewSet(mixins.CreateModelMixin,
                     mixins.DestroyModelMixin,
                     mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = Process.objects.filter()
    serializer_class = ProcessSerializer

    def list(self, request):
        qs = Process.objects.filter()

        if not request.user.is_superuser:
            qs = qs.filter(company=request.user.company)

        return Response(self.serializer_class(qs, many=True, context={'request': request}).data)
