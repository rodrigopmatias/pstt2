# -*- coding: utf-8 -*-
import json
import hashlib

from urllib.request import Request, urlopen
from urllib.error import HTTPError
from django.db import models
from django.contrib.auth.models import (
    PermissionsMixin,
    BaseUserManager
)
from django.contrib.auth.base_user import AbstractBaseUser
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from .middleware import CurrentUser


class Company(models.Model):
    name = models.CharField(verbose_name=_('Name'), max_length=80, unique=True)
    hook = models.CharField(verbose_name=_('API Resource'), max_length=200)
    authorization = models.CharField(verbose_name=_('Autorization Resource'), max_length=100)

    class Meta:
        verbose_name = _('Company')

    def __str__(self):
        return self.name


class CustomUserManager(BaseUserManager):

    def create_user(self, username, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not username:
            raise ValueError('Users must have an username')

        user = self.model(
            username=username
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            username,
            password=password
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(_('username'), max_length=60, unique=True)
    email = models.CharField(_('email'), max_length=120, unique=True)
    first_name = models.CharField(_('First name'), max_length=60)
    last_name = models.CharField(_('Last name'), max_length=120)
    company = models.ForeignKey(Company, verbose_name=_('Company'), related_name='users', null=True, blank=True)
    is_staff = models.BooleanField(_('Is staff'), default=False)

    USERNAME_FIELD = 'username'

    objects = CustomUserManager()

    class Meta:
        verbose_name = _('User')

    def get_short_name(self):
        return self.first_name

    def get_full_name(self):
        return ' '.join([self.first_name, self.last_name])


class Process(models.Model):
    company = models.ForeignKey(Company, verbose_name=_('Company'), related_name='processes')
    process_number = models.CharField(_('process number'), max_length=20, unique=True)
    process_data = models.TextField(_('process informations'))
    hash_process_data = models.CharField(_('sign'), max_length=32, null=True, blank=True)

    class Meta:
        verbose_name = _('Process')

    def __str__(self):
        return self.process_number

    def notify_company(self):
        notification = Notification(
            hook=self.company.hook,
            authorization=self.company.authorization,
            status=0
        )

        self.notifications.add(notification, bulk=False)
        '''
        TODO: seria melhor fazer esta tarefa usando o celery
        '''
        notification.send()

    @property
    def is_dirty(self):
        if self.pk:
            older = self.__class__.objects.get(pk=self.pk)
            return older.hash_process_data != hashlib.new('md5', self.process_data.encode()).hexdigest()
        else:
            return True

    def save(self, *args, **kwargs):
        if not hasattr(self, 'company') and not self.pk:
            user = CurrentUser.get()
            self.company = user.company if user else None

        self.hash_process_data = hashlib.new('md5', self.process_data.encode()).hexdigest()
        pending_notification = self.is_dirty

        super(Process, self).save(*args, **kwargs)

        if pending_notification:
            self.notify_company()


class Notification(models.Model):
    process = models.ForeignKey(Process, related_name='notifications')
    hook = models.CharField(max_length=200)
    authorization = models.CharField(max_length=100)
    at_moment = models.DateTimeField(auto_now_add=True)
    by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='+', null=True, blank=True)
    status = models.IntegerField()

    class Meta:
        ordering = ('-at_moment',)

    def send(self):
        req = Request(
            self.process.company.hook,
            data=json.dumps({'changed': 'data'}).encode(),
            headers={
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': self.process.company.authorization
            }
        )

        try:
            with urlopen(req) as resp:
                self.status = resp.status
        except HTTPError as e:
            self.status = e.code

        self.save()

    def save(self, *args, **kwargs):
        if not self.pk and not self.by:
            self.by = CurrentUser.get()

        super(Notification, self).save(*args, **kwargs)
